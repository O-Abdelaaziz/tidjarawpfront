import { ProductlistDetailsComponent } from './components/productlist-details/productlist-details.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductlistComponent } from './components/productlist/productlist.component';


const routes: Routes = [
  {path: '', redirectTo:"/products" ,pathMatch:'full'},
  {path: 'products', component: ProductlistComponent},
  {path: 'search/:name', component: ProductlistComponent},
  {path: 'category/:id', component: ProductlistComponent},
  {path: 'products/:id', component: ProductlistDetailsComponent},
  // {path: 'cartdetails', component: CartDetailsComponent},

  {path: '**', redirectTo: '/notfound', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
