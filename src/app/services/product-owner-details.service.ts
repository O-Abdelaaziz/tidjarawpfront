import { ProductOwnerDetail } from './../models/product-owner-detail';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

interface GetEmbeddedOwnerDetails{
  _embedded: {
    productOwnerDetails: ProductOwnerDetail[];
  }
}

@Injectable({
  providedIn: 'root'
})
export class ProductOwnerDetailsService {

  baseUrl:string="http://192.168.40.98:8081/productOwnerDetails";
  //http://192.168.40.98:8081/productOwnerDetails/search/findByOnwerId?ownerId=259

   constructor(private http: HttpClient) { }

   getOwnerInfoByProductId(productId:number): Observable<GetEmbeddedOwnerDetails> {
    const searchUrl=`${this.baseUrl}/search/findByProductId?productId=${productId}`;
    return this.http.get<GetEmbeddedOwnerDetails>(searchUrl);
  }

  getOwnerInfoByOwnerId(ownerId:number): Observable<GetEmbeddedOwnerDetails> {
    const searchUrl=`${this.baseUrl}/search/findByOnwerId?ownerId=${ownerId}`;
    return this.http.get<GetEmbeddedOwnerDetails>(searchUrl);
  }
}
