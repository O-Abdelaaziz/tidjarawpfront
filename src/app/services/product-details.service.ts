import { Observable } from 'rxjs';
import { ProductDetail } from './../models/product-detail';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

interface GetEmbeddedProductDetails{
  _embedded: {
    productDetails: ProductDetail[];
  }
}

@Injectable({
  providedIn: 'root'
})
export class ProductDetailsService {

  baseUrl:string="http://192.168.40.98:8081/productDetails";

   constructor(private http: HttpClient) { }

   getAllProductByProductId(productId:number): Observable<GetEmbeddedProductDetails> {
    const searchUrl=`${this.baseUrl}/search/findByProductId?productId=${productId}`;
    return this.http.get<GetEmbeddedProductDetails>(searchUrl);
  }

}
