import { map } from 'rxjs/operators';
import { Productlist } from './../models/product-list';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


interface GetEmbeddedBooks {
  _embedded: {
    productLists: Productlist[];
  },
  page:{
    size:number,
    totalElements:number,
    totalPages:number,
    number:number
  }
}

@Injectable({
  providedIn: 'root'
})
export class ProductlistService {

   // baseUrl:string="http://localhost:8080/api/categories?sort=id,asc";
   baseUrl:string="http://192.168.40.98:8081/productLists";

   constructor(private http: HttpClient) { }
 
  //  getAllProductList(): Observable<Productlist[]> {
  //    return this.http.get<GetEmbeddedBooks>(this.baseUrl).pipe(
  //      map(response=>response._embedded.productLists)
  //    );
  //  }

   getAllProductList(categoryId:number,currentPage:number,pageSize:number): Observable<GetEmbeddedBooks> {
    const searchUrl=`${this.baseUrl}/search/findByIdcategorie?idcategorie=${categoryId}&page=${currentPage}&size=${pageSize}`;
    return this.http.get<GetEmbeddedBooks>(searchUrl);
  }

  getAllProductListByName(name:string,currentPage:number,pageSize:number):Observable<GetEmbeddedBooks>{
    const searchUrl=`${this.baseUrl}/search/findByPostTitleContainingIgnoreCase?name=${name}&page=${currentPage}&size=${pageSize}`;
    return this.http.get<GetEmbeddedBooks>(searchUrl);
  }

  getProductListById(id:number):Observable<Productlist>{
    const searchUrl=`${this.baseUrl}/${id}`;
    return this.http.get<Productlist>(searchUrl);
  }
}
