import { Category } from './../models/category-list';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface GetEmbeddedCategories{
  _embedded: {
    categoryLists: Category[];
  },
  page:{
    size:number,
    totalElements:number,
    totalPages:number,
    number:number
  }
} 

@Injectable({
  providedIn: 'root'
})
export class CategorylistService {

   // baseUrl:string="http://localhost:8080/api/categories?sort=id,asc";
   //baseUrl:string="http://192.168.40.98:8081/categoryLists";
   baseUrl:string="http://192.168.40.98:8081/categoryLists/search/findByIdCategorieParant?parenid=0";

   constructor(private http: HttpClient) { }
 
  //  getAllCategories(): Observable<Category[]> {
  //    return this.http.get<GetEmbeddedCategories>(this.baseUrl).pipe(
  //      map(response=>response._embedded.categoryLists)
  //    );
  //  }


   getAllCategories(): Observable<GetEmbeddedCategories> {
    const searchUrl=`${this.baseUrl}`;
    return this.http.get<GetEmbeddedCategories>(searchUrl);
  }


  getAllParentsCategories(categoryParentId:number): Observable<Category[]> {
    const searchUrl=`http://192.168.40.98:8081/categoryLists/search/findByIdCategorieParant?parenid=${categoryParentId}`;
    return this.http.get<GetEmbeddedCategories>(searchUrl).pipe(
      map(response=>response._embedded.categoryLists)
    );
  }

}
