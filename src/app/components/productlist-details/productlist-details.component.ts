import { ProductOwnerDetail } from './../../models/product-owner-detail';
import { ProductOwnerDetailsService } from './../../services/product-owner-details.service';
import { ProductDetail } from './../../models/product-detail';
import { ProductDetailsService } from './../../services/product-details.service';
import { ProductlistService } from './../../services/productlist.service';
import { ActivatedRoute } from '@angular/router';
import { Productlist } from './../../models/product-list';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-productlist-details',
  templateUrl: './productlist-details.component.html',
  styleUrls: ['./productlist-details.component.css'],
})
export class ProductlistDetailsComponent implements OnInit {
  productlist: Productlist = new Productlist();
  productlistRendom: Productlist = new Productlist();
  productDetails: ProductDetail[];
  productOwnerDetail: ProductOwnerDetail[];

  productlistId: number;
  mapProduct = new Map<string, string>();
  mapOwner = new Map<string, string>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private productDetailsService: ProductDetailsService,
    private productOwnerDetailsService: ProductOwnerDetailsService,
    private productlistservice: ProductlistService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(() => {
      this.selectBookById();
      this.selectProductDetailById();
      this.selectRendomById();
      //this.selectOwnerDetailById();
    });
  }

  selectBookById() {
    // const hasBookId = +this.activatedRoute.snapshot.paramMap.has("id")
    // if (hasBookId) {
    this.productlistId = +this.activatedRoute.snapshot.paramMap.get('id');
    // }
    console.log('id ' + this.productlistId);

    this.productlistservice
      .getProductListById(this.productlistId)
      .subscribe((book) => {
        this.productlist = book;
        console.log('book ' + this.productlist);
      });
  }

  selectRendomById() {
    const rendomId = this.randomIntFromInterval(14800, 14800);
    console.log('rendomId' + rendomId);

    this.productlistservice.getProductListById(rendomId).subscribe((book) => {
      this.productlistRendom = book;
    });
  }

  selectProductDetailById() {
    this.productDetailsService
      .getAllProductByProductId(this.productlistId)
      .subscribe((book) => {
        this.productDetails = book._embedded.productDetails;
          //console.log('productDetails' + this.productDetails);
        for (let p of this.productDetails) {
          this.mapProduct.set(p.metaKey, p.metaValue);
          //console.log(this.mapProduct);
        }
      });
  }

  selectOwnerDetailById(id: number) {
    this.productOwnerDetailsService
      .getOwnerInfoByOwnerId(id)
      .subscribe((ownerDetails) => {
        this.productOwnerDetail = ownerDetails._embedded.productOwnerDetails;
          console.log('productOwnerDetail' + this.productOwnerDetail);

        for (let p of this.productOwnerDetail) {
          this.mapOwner.set(p.metaKey, p.metaValue);
          //console.log("mapOwner" +this.mapOwner);          
        }
      });
  }

  randomIntFromInterval(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}
