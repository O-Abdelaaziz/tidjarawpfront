import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { Category } from './../../models/category-list';
import { Component, OnInit } from '@angular/core';
import { CategorylistService } from 'src/app/services/categorylist.service';

@Component({
  selector: 'app-categorylist',
  templateUrl: './categorylist.component.html',
  styleUrls: ['./categorylist.component.css']
})
export class CategorylistComponent implements OnInit {

  listcategories :Category[];
  listCategoriesParent :Category[]=[];

  currentPage: number = 1;
  pageSize: number = 20;
  totalItems: number = 0;
  cateParentId:number ;
  constructor(private categoriesService: CategorylistService,
    private ngbPaginationConfig: NgbPaginationConfig) {

      ngbPaginationConfig.maxSize = 3;
      ngbPaginationConfig.boundaryLinks = true;
     }

  ngOnInit(): void {
    this.selectAllCategories();
  }

  selectAllCategories(){
    this.categoriesService.getAllCategories().subscribe(
      (categories)=>{
        this.listcategories=categories._embedded.categoryLists;
        // this.currentPage = categories.page.number + 1;
        // this.totalItems = categories.page.totalElements;
        // this.pageSize = categories.page.size;
      }
    );
  }

  selectAllCategoriesParentId(cateParentId:number){
    this.categoriesService.getAllParentsCategories(cateParentId).subscribe(
      (categoriesparent)=>{
        this.listCategoriesParent=categoriesparent;
        console.log(" this.listCategoriesParent "+categoriesparent.length);
        
      }
    );
  }

  updatePageSize(pageSize: number) {
    this.pageSize = pageSize;
    this.currentPage = 1;
    this.selectAllCategories();
  }
}
