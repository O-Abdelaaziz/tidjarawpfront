import { ProductlistService } from './../../services/productlist.service';
import { Productlist } from './../../models/product-list';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {

  productlists :Productlist[];
  categoryId: number;
  postTitle: string;
  searchMode: boolean;

  currentPage: number = 1;
  pageSize: number = 5;
  totalItems: number = 0;
  previousCategory: number = 2021;


  constructor(private productListService:ProductlistService,
    private activatedRoute: ActivatedRoute,
    private spinnerService: NgxSpinnerService,
    private router:Router,
    private ngbPaginationConfig: NgbPaginationConfig) {

      ngbPaginationConfig.maxSize = 3;
    ngbPaginationConfig.boundaryLinks = true;
     }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(
      () => {
        this.selectAllProducts();
      }
    );
  }

  selectAllProducts() {
    this.spinnerService.show();
    this.searchMode = this.activatedRoute.snapshot.paramMap.has('name');
    console.log("this.searchMode " +this.searchMode);
    
    if (this.searchMode) {
      this.selectAllBooksByName();
    }else{
      this.selectAllProductsById();
    }
  }

  selectAllProductsById(){

    const hasCategoryId: boolean = this.activatedRoute.snapshot.paramMap.has('id');
    console.log("has "+hasCategoryId);

    if (hasCategoryId) {
      this.categoryId = +this.activatedRoute.snapshot.paramMap.get('id');
    } else {
      this.categoryId = 2021;
    }
    
    if (this.previousCategory != this.categoryId) {
      this.currentPage = 1;
    }

    this.previousCategory = this.categoryId;
    this.productListService.getAllProductList(this.categoryId,this.currentPage - 1, this.pageSize).subscribe(
      this.loadBookWithSppiner()
    )
  }


  selectAllBooksByName() {
    this.postTitle = this.activatedRoute.snapshot.paramMap.get('name');
    console.log("book name "+this.postTitle);

    this.productListService.getAllProductListByName(this.postTitle,this.currentPage - 1, this.pageSize).subscribe(
      this.loadBookWithSppiner()
    );
  }



  loadBookWithSppiner() {
    return (allproducts) => {
      console.log("allproducts" + allproducts);
      
      this.spinnerService.hide();
      this.productlists = allproducts._embedded.productLists;
      this.currentPage = allproducts.page.number + 1;
      this.totalItems = allproducts.page.totalElements;
      this.pageSize = allproducts.page.size;
    }
  }


  updatePageSize(pageSize: number) {
    this.pageSize = pageSize;
    this.currentPage = 1;
    this.selectAllProducts();
  }

}
